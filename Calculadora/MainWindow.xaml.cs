﻿using Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculadora
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            CargarComboBox();
        }

        private void CargarComboBox()
        {
            ComboBoxOperacion.Items.Add("Suma");
            ComboBoxOperacion.Items.Add("Sustracción");
            ComboBoxOperacion.Items.Add("Multiplicación");
            ComboBoxOperacion.Items.Add("División");
            ComboBoxOperacion.SelectedIndex = 0;
        }

        private void BotonCalcular_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LabelResultado.Content = "Resultado: " + ResultadoOperacion();
            }
            catch (FormatException excepcionFormato)
            {
                MessageBox.Show(excepcionFormato.Message);
            }
        }

        public string ResultadoOperacion()
        {
            Separador separador = new Separador();
            Operaciones operacion = new Operaciones();
            List<double> ListaOperaciones = separador.ConvertirListaDouble(separador.Separar(TextBoxOperacion.Text));
            String operacionSeleccionada = ComboBoxOperacion.SelectedItem.ToString();
            if (operacionSeleccionada == "Suma")
            {
                return operacion.Sumar(ListaOperaciones).ToString();
            }
            if (operacionSeleccionada == "Sustracción")
            {
                return operacion.Restar(ListaOperaciones).ToString();
            }
            if (operacionSeleccionada == "Multiplicación")
            {
                return operacion.Multiplicar(ListaOperaciones).ToString();
            }
            else
            {
                return operacion.Dividir(ListaOperaciones).ToString();
            }
        }
    }
}
