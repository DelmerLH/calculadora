﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Operaciones
    {
        public double Sumar(List<double> ListaOperaciones)
        {
            double resultado = 0;
            for(int i=0; i < ListaOperaciones.Count; i++)
            {
                resultado += ListaOperaciones[i]; 
            }
            return resultado;
        }

        public double Restar(List<double> ListaOperaciones)
        {
            double resultado = ListaOperaciones[0];
            for (int i = 1; i < ListaOperaciones.Count; i++)
            {
                resultado -= ListaOperaciones.ElementAt(i);
            }
            return resultado;
        }

        public double Multiplicar(List<double> ListaOperaciones)
        {
            double resultado = 1;
            for (int i = 0; i < ListaOperaciones.Count; i++)
            {
                resultado *= ListaOperaciones.ElementAt(i);
            }
            return resultado;
        }

        public double Dividir(List<double> ListaOperaciones)
        {
            double resultado = ListaOperaciones[0];
            for (int i = 1; i < ListaOperaciones.Count; i++)
            {
                resultado /= ListaOperaciones.ElementAt(i);
            }
            return resultado;
        }
    }
}
