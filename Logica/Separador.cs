﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Separador
    {
        public List<String> Separar(String operacion)
        {
            List<String> listaOperacion = new List<String>();
            char[] separadores = {','};
            try
            {
                listaOperacion = operacion.Split(separadores).ToList();
            } catch (FormatException excepcionFormato)
            {
                throw excepcionFormato;
            }
            return listaOperacion;
        }

        public List<double> ConvertirListaDouble(List<String> operacion)
        {
            List<double> ListaDoubles = new List<double>();
            try
            {
                ListaDoubles = operacion.Select(x => double.Parse(x)).ToList();
            } catch (InvalidCastException excepcionCasteo)
            {
                throw excepcionCasteo;
            }
           
            return ListaDoubles;
        }
    }
}
