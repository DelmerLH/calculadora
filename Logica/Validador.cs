﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    class Validador
    {
        public bool OperadorValido(String operador)
        {
            if (operador == "+")
            {
                return true;
            }
            else if (operador == "-")
            {
                return true;
            }
            else if (operador == "*")
            {
                return true;
            }
            else if (operador == "/")
            {
                return true;
            }
            else if (operador == "%")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool NumeroValido(string numero)
        {
            if (double.TryParse(numero.ToString(), out double resultado))
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
