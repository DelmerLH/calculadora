﻿using System;
using System.Collections.Generic;
using Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LogicaTest
{
    [TestClass]
    public class SeparadorTest
    {
        [TestMethod]
        public void SepararIndice0Test()
        {
            Separador separar = new Separador();
            String CadenaOperacion = "12,12,1";
            List<String> ListaOperacion = separar.Separar(CadenaOperacion);
            Assert.AreEqual("12", ListaOperacion[0]);
        }

        [TestMethod]
        public void SepararIndice1Test()
        {
            Separador separar = new Separador();
            String CadenaOperacion = "12,12,1";
            List<String> ListaOperacion = separar.Separar(CadenaOperacion);
            Assert.AreEqual("12", ListaOperacion[1]);
        }

        [TestMethod]
        public void SepararIndice2Test()
        {
            Separador separar = new Separador();
            String CadenaOperacion = "12,12,1";
            List<String> ListaOperacion = separar.Separar(CadenaOperacion);
            Assert.AreEqual("1", ListaOperacion[2]);
        }


        [TestMethod]
        public void ConvertirListaDoubleTest()
        {
            Separador separar = new Separador();
            String CadenaOperacion = "12,12,1";
            List<String> ListaOperacion = separar.Separar(CadenaOperacion);
            List<double> ListaCifras = separar.ConvertirListaDouble(ListaOperacion);
            Assert.AreEqual(12, ListaCifras[0]);
        }
    }
}
